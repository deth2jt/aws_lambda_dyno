resource "random_id" "my-random-id" {
    byte_length = 2
}

resource "aws_s3_bucket" "my-foo-bucket" {
    bucket = "${var.s3_buck_name}-${random_id.my-random-id.dec}"
    acl = "private"

    versioning {
        enabled = true
    }

    lifecycle_rule {
        enabled = true

        transition {
            storage_class = "STANDARD_IA"
            days = 30
        }
    }

    tags = {
        Name = "foobar-s3-tagged"
    }

}


# Upload an object
resource "aws_s3_bucket_object" "object" {

    bucket = aws_s3_bucket.my-foo-bucket.bucket
    //TODO
    key    = "json_verify"

    acl    = "private"  # or can be "public-read"

    #source = "../terrajs.zip"
    source    = "terrajs.zip"
    //etag = filemd5("../terrajs.zip")
    etag = "${md5("terrajs.zip")}"
}