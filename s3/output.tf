output "s3_buck"  {
    value = "${aws_s3_bucket.my-foo-bucket.bucket}"
}

output "s3_buck_key"  {
    value = "${aws_s3_bucket_object.object.key}"
}