
provider "aws" {
  region = "us-east-1"
}


module "s3" {
  source       = "./s3"
  s3_buck_name = "foo-buck-s3"
}

resource "aws_dynamodb_table" "foodb" {
  name           = "foodbname"
  hash_key       = "id"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5

  attribute {
    name = "id"
    type = "S"
  }
}


resource "aws_iam_role_policy" "lambda_policy" {
  name = "lambda_policy"
  role = aws_iam_role.role_for_LDC.id

  policy = file("policy.json")

}


resource "aws_iam_role" "role_for_LDC" {
  name = "myrole"

  assume_role_policy = file("assume_role_policy.json")
}


resource "aws_lambda_function" "fooLambda" {

  function_name = "foofunc"
  s3_bucket     = module.s3.s3_buck
  s3_key        = module.s3.s3_buck_key
  role          = aws_iam_role.role_for_LDC.arn
  handler       = "tr_lamaba.handler"
  runtime       = "nodejs12.x"
}

resource "aws_api_gateway_rest_api" "apiLambda" {
  name = "fooAPI"
}

resource "aws_api_gateway_resource" "Resource" {
  rest_api_id = aws_api_gateway_rest_api.apiLambda.id
  parent_id   = aws_api_gateway_rest_api.apiLambda.root_resource_id
  path_part   = "myresource"
}

resource "aws_api_gateway_method" "Method" {
  rest_api_id   = aws_api_gateway_rest_api.apiLambda.id
  resource_id   = aws_api_gateway_resource.Resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambaInt" {
  rest_api_id = aws_api_gateway_rest_api.apiLambda.id
  resource_id = aws_api_gateway_resource.Resource.id
  http_method = aws_api_gateway_method.Method.http_method

  integration_http_method = aws_api_gateway_method.Method.http_method
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.fooLambda.invoke_arn
}


resource "aws_api_gateway_deployment" "apideploy" {
  depends_on  = [aws_api_gateway_integration.lambaInt]
  rest_api_id = aws_api_gateway_rest_api.apiLambda.id
  stage_name  = "Prod"
}
//aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId]' --region ap-southeast-2 --output text | wc -l

resource "aws_lambda_permission" "apigw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.fooLambda.function_name
  principal     = "apigateway.amazonaws.com"

  #source_arn = "${aws_api_gateway_rest_api.apiLambda.execution_arn}/Prod/POST/myresource"
  source_arn = "${aws_api_gateway_rest_api.apiLambda.execution_arn}/*/POST/myresource"
}


output "baseurl" {
  value = aws_api_gateway_deployment.apideploy.invoke_url
}
